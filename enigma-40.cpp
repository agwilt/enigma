#include "enigma_base.hpp"
#include "enigma_args.hpp"

#include <cstdio>
#include <iostream>

int main(int argc, char *argv[])
{
	Enigma enigma = enigma_from_command_line_args(argc, argv, false);

	// Read Grunstellung (plaintext)
	std::vector<char> grundstellung(enigma.rotors.size(), '?');
	for (size_t i = 0; i < enigma.rotors.size(); ++i) {
		std::putchar(grundstellung[i] = get_key_letter());
	}
	if (std::getchar() != '\n') {
		std::cerr << "Error: Expected newline after Grundstellung\n";
		exit(1);
	}
	std::putchar('\n');

	// Set Grundstellung
	for (size_t i = 0; i < enigma.rotors.size(); ++i) {
		enigma.rotors[i].set_position(grundstellung[i]);
	}

	// Read message key
	std::vector<char> message_key(enigma.rotors.size(), '?');
	for (size_t i = 0; i < enigma.rotors.size(); ++i) {
		std::putchar(message_key[i] = enigma.type(get_key_letter()));
	}

	// Set message key
	for (size_t i = 0; i < enigma.rotors.size(); ++i) {
		enigma.rotors[i].set_position(message_key[i]);
	}

	enigma.type_stream();

	return 0;
}
