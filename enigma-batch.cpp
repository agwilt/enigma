#include "enigma_base.hpp"
#include "enigma_args.hpp"

#include <cstdio>

int main(int argc, char *argv[])
{
	Enigma enigma = enigma_from_command_line_args(argc, argv, true);

	enigma.type_stream();

	return 0;
}
