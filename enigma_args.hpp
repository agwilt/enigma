#ifndef ENIGMA_ARGS_H
#define ENIGMA_ARGS_H

#include "enigma_base.hpp"
#include <string>

bool isupperchar(std::string const& s);

Enigma enigma_from_command_line_args(int argc, char *argv[], bool expect_grundstellung);

#endif // ENIGMA_ARGS_H
