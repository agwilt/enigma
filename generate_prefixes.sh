#!/bin/sh

ETW=ABC
UKW=UKW-A
ROTORS="I II III"
RINGSETTINGS="AAA"
GRUNDSTELLUNG="QWE"
STECKER="QW ER TY UI OP AS"

INNER_SETTINGS="3 $ETW $UKW $ROTORS $RINGSETTINGS"

while true
do
	KEY=$(pwgen -0 -A 3 1 | tr '[:lower:]' '[:upper:]')
	if [ "$1" = 30 ]; then
		echo $KEY$KEY | ./enigma-batch $INNER_SETTINGS $GRUNDSTELLUNG $STECKER || exit
	elif [ "$1" = 38 ]; then
		GRUNDSTELLUNG=$(pwgen -0 -A 3 1 | tr '[:lower:]' '[:upper:]')
		printf "$GRUNDSTELLUNG " || exit
		echo $KEY$KEY | ./enigma-batch $INNER_SETTINGS $GRUNDSTELLUNG $STECKER || exit
	elif [ "$1" = 40 ]; then
		GRUNDSTELLUNG=$(pwgen -0 -A 3 1 | tr '[:lower:]' '[:upper:]')
		printf "$GRUNDSTELLUNG " || exit
		echo $KEY | ./enigma-batch $INNER_SETTINGS $GRUNDSTELLUNG $STECKER || exit
	else
		echo "Usage: $0 procedure num_messages"
		echo " procedure: key procedure, one of:"
		echo "  30: 1930 procedure, message key doubled and encrypted with day key."
		echo "  38: 1938 procedure, random Grundstellung used to encipher doubled message key. Grundstellung transmitted en clair in preamble."
		echo "  40: 1940 procedure, as 38 but message key only typed once."
		echo " num_messages: number of messages to generate"
		exit 1
	fi
done
