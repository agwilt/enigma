#include "enigma_base.hpp"

#include <map>
#include <iostream>
#include <cassert>
#include <cctype>
#include <cstdio>

char get_key_letter()
{
	char input = std::getchar();

	if (input == ' ' or input == '\t') { // Skip whitespace
		std::putchar(' ');
		return get_key_letter();
	}

	if (std::islower(input)) { // Convert lower to upper case
		input = input - 'a' + 'A';
	}
	if (not std::isupper(input)) {
		std::cerr << "\nError: Malformed message: Expected key letter, not " << input << "\n";
		exit(1);
	}
	return input;
};

static std::string descr(Pos const x)
{
	char *buffer = NULL;
	asprintf(&buffer, "%d/%c/%02d", x, 'A'+x, x+1);
	return buffer;

	//return std::format("{}/{:c}/{:0>2}", x, 'A' + x, x+1);
}

PosArray inverse(PosArray const& a)
{
	PosArray inverted;
	inverted.fill(26);

	for (int i = 0; i < 26; ++i)
		inverted.at(a[i]) = i;

	for (auto const& c : inverted)
		assert(c != 26);

	return inverted;
}

RotorType::RotorType(PosArray const wiring, FlagArray const turnovers)
 : wiring{wiring}, inverse_wiring{inverse(wiring)}, turnovers{turnovers} {}

RotorType RotorType::etw_from_key_order(std::string const& inverse_wiring)
{
	auto const wiring = inverse(wiring_from_str(inverse_wiring, 0));
	assert(inverse(wiring) == wiring_from_str(inverse_wiring, 0));
	return RotorType(wiring, {});
}

RotorType RotorType::rotor(std::string const& wiring_str, std::vector<char> const& turnovers)
{
	return RotorType{wiring_from_str(wiring_str, 1), turnovers_from_letters(turnovers)};
}

RotorType RotorType::reflector(std::string const& wiring_str)
{
	return RotorType{wiring_from_str(wiring_str, 0), {}};
}

PosArray RotorType::wiring_from_str(std::string const& s, Pos const pos_of_a_on_wheel)
{
	// Note: for some reason, ETW and UKW are given by pos_of_a_on_wheel=0,
	// and other rotors by pos_of_a_on_wheel=1
	assert(s.size() == 26);
	PosArray array;
	for (int i = 0; i < 26; ++i) {
		assert(std::isupper(s[i]));
		array[(i + pos_of_a_on_wheel) % 26] = (s[i] + pos_of_a_on_wheel - 'A') % 26;
	}
	return array;
}

FlagArray RotorType::turnovers_from_letters(std::vector<char> const& s)
{
	// Note: we take the post-turnover letter in the input, but internally save the pre-turnover letter
	FlagArray turnovers{false};
	for (auto const& c : s) {
		assert(std::isupper(c));
		turnovers[(c - 'A' + 25) % 26] = true;
	}
	return turnovers;
}

Pos Rotor::fwd(Pos const c) const
{
	return (_type.wiring[(c + _position) % 26] - _position + 26) % 26;
}

Pos Rotor::back(Pos const c) const
{
	return (_type.inverse_wiring[(c + _position) % 26] - _position + 26) % 26;
}

void Rotor::set_ring(char const setting)
{
	// Ring setting describes at which letter on the ring the rotor's "1" is.
	assert(std::isupper(setting));
	_ring_pos = ('B' - setting + 26) % 26;
	assert(ring_setting() == setting);
}

void Rotor::set_position(char const letter_in_window)
{
	assert(std::isupper(letter_in_window));
	unsigned const letter_offset = letter_in_window - 'A';
	_position = (letter_offset + _ring_pos) % 26;
	assert(window() == letter_offset);
}

unsigned Rotor::window() const
{
	return (26 + _position - _ring_pos) % 26;
}

char Enigma::substitution(char const c, bool const detailed) const
{
	assert(std::isupper(c));
	if (detailed) std::cout << c;

	Pos x = steckerbrett[static_cast<Pos>(c - 'A')];
	if (detailed) std::cout << " -Steckerbrett-> " << static_cast<char>('A' + x);

	x = eintrittswalze.fwd(x);
	if (detailed) std::cout << " -ETW-> " << descr(x);

	for (auto rotor = rotors.rbegin(); rotor != rotors.rend(); ++rotor) {
		x = rotor->fwd(x);
		if (detailed) std::cout << " -> " << descr(x);
	}

	assert(reflector.fwd(x) == reflector.back(x));
	x = reflector.fwd(x);
	if (detailed) std::cout << " -UKW-> " << descr(x);

	for (auto rotor = rotors.begin(); rotor != rotors.end(); ++rotor) {
		x = rotor->back(x);
		if (detailed) std::cout << " -> " << descr(x);
	}

	x = eintrittswalze.back(x);
	if (detailed) std::cout << " -ETW⁻¹-> " << static_cast<char>('A' + x);

	x = steckerbrett[x];
	if (detailed) std::cout << " -Steckerbrett-> " << static_cast<char>('A' + x) << "\n";

	return 'A' + x;
}

void Enigma::step()
{
	if (rotors.empty()) {
		if (step_reflector) {
			reflector.step();
		}
		return;
	}

	std::vector<bool> will_step(rotors.size(), false);

	unsigned i = rotors.size() - 1; // index of current rotor

	will_step[i] = true; // always step rightmost rotor

	for (; i > 0; --i) {
		// see if we step out left neighbour (which incidentally also steps us)
		if (rotors[i].turnover()) {
			// did we trigger a turnover with this rotor's notched ring?
			will_step[i-1] = will_step[i] = true;
		}
	}

	if (step_reflector and rotors[0].turnover()) {
		will_step[0] = true;
		reflector.step();
	}

	for (i = 0; i < rotors.size(); ++i)
		if (will_step[i])
			rotors[i].step();
}

Enigma::Enigma(RotorType const& etw_type, RotorType const& ukw_type, bool const step_reflector)
 :
	 eintrittswalze{etw_type},
	 rotors{},
	 reflector{ukw_type},
	 step_reflector{step_reflector}
{
	for (size_t i = 0; i < steckerbrett.size(); ++i)
		steckerbrett[i] = i;
}

bool Enigma::unsteck(Pos const a, Pos const b)
{
	for (auto const& x : {a, b}) {
		if (steckerbrett.at(x) != a+b-x) {
			std::cerr << "Warning: " << a << " and " << b << " were not steckered to eachother in the first "
			          << "place, aborting...\n";
			return false;
		}
	}
	steckerbrett.at(a) = a;
	steckerbrett.at(b) = b;
	return true;
}

void Enigma::steck(Pos const a, Pos const b)
{
	for (auto const& x : {a, b}) {
		if (steckerbrett.at(x) != x) {
			std::cerr << "Warning: Un-stecked pair " << x << ", " << steckerbrett.at(x) << "\n";
			assert(steckerbrett.at(steckerbrett.at(x)) == x);
			steckerbrett.at(steckerbrett.at(x)) = steckerbrett.at(x);
			steckerbrett.at(x) = x;
		}
	}
	steckerbrett.at(a) = b;
	steckerbrett.at(b) = a;
}

void Enigma::run()
{
	std::cout << "Note: This simulates an Enigma with \"Antriebstaste\"; the Enigma will step when the Enter"
		      << " key is pressed.\n";

	auto const prompt = [&]() {
		std::cout << "[" << reflector.window_letter() << "] ";
		for (auto const& rotor : rotors) {
			std::cout << "[" << rotor.window_letter() << "]";
		}
		std::cout << " " << std::flush;
	};

	for (std::string line; prompt(), std::getline(std::cin, line); ) {
		for (auto const& c : line) {
			if (std::isupper(c)) {
				std::cout << substitution(c);
			} else if (std::islower(c)) {
				std::cout << substitution(c - 'a' + 'A');
			} else {
				std::cout << c;
			}
		}
		std::cout << "\n";
		step();
	}

	std::cout << "\n";
}

void Enigma::type_stream(FILE *in, FILE *out)
{
	for (char c; (c = std::getc(in)) != EOF; ) {
		if (std::islower(c)) {
			c = c - 'a' + 'A';
		}
		if (std::isupper(c)) {
			std::putc(type(c), out);
		} else {
			std::putc(c, out);
		}
	}
}
