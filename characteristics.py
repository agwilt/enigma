#!/usr/bin/env python3

import sys
import string

def index(c):
    assert c in string.ascii_uppercase
    return ord(c) - ord('A')

def letter(i):
    assert i >= 0 and i < 26
    return chr(ord('A') + i)

def exit_usage():
    print("Usage: %s a[ll]|p[ermutations]|c[ycles]|l[engths]" % sys.argv[0])
    exit(1)

def print_permutations(permutations):
    print(string.ascii_uppercase)
    print("-"*26)
    for p in permutations:
        print("".join(p))

def print_cycles(cycles):
    for cycle in cycles:
        print("(" + "".join(cycle) + ")", end='')
    print("")

def read_permutations():
    permutations = [['?' for i in range(26)] for j in range(3)]
    num_set = [0 for j in range(3)]
    for line in sys.stdin:
        if min(num_set) >= 25:
            break
        if len(line) != 2*3+1:
            print("Error: Each input line must consist of 6 consecutive upper-case letters")
            exit(1)
        for i in range(3):
            ind = index(line[i])
            mapsto = line[3+i]
            if permutations[i][ind] == '?':
                permutations[i][ind] = mapsto
                num_set[i] += 1
            else:
                assert permutations[i][ind] == mapsto
    sys.stdin.close()
    for p in permutations:
        almost_complete = (p.count('?') == 1)
        for c in string.ascii_uppercase:
            assert p.count(c) <= 1
            if almost_complete and p.count(c) == 0:
                p[p.index('?')] = c
                almost_complete = False
    return permutations

def characteristic_cycles(permutations):
    all_cycles = []
    for p in permutations:
        cycles = []
        covered = [False for j in range(26)]
        while False in covered:
            x = covered.index(False)
            cycle = [letter(x)]
            covered[x] = True
            while p[x] != cycle[0]:
                assert p[x] not in cycle
                next_x_char = p[x]
                if next_x_char == '?':
                    print("Error: Insufficient information to build cycles:", p)
                    exit(1)
                cycle.append(next_x_char)
                x = index(next_x_char)
                covered[x] = True
            cycles.append(cycle)
        all_cycles.append(cycles)
    return all_cycles

def sorted_cycle_length_pairs(cycles):
    l = sorted([len(c) for c in cycles], reverse=True)
    assert l[::2] == l[1::2]
    return l[::2]

def print_characteristic_cycles(all_cycles):
    for cycles in all_cycles:
        print_cycles(cycles)

def print_characteristic_cycle_length_pairs(all_cycles):
    for cycles in all_cycles:
        print(",".join((str(i) for i in sorted_cycle_length_pairs(cycles))))

if __name__ == "__main__":
    if len(sys.argv) != 2:
        exit_usage()
    mode = sys.argv[1]
    if len(mode) == 0:
        exit_usage()

    permutations = read_permutations()

    if "all".startswith(mode):
        print("Permutations:")
        print_permutations(permutations)
        print("\nCharacteristic Cycles:")
        all_cycles = characteristic_cycles(permutations)
        print_characteristic_cycles(all_cycles)
        print("\nCharacteristic Cycle Length Pairs:")
        print_characteristic_cycle_length_pairs(all_cycles)
    elif "permutations".startswith(mode):
        print_permutations(permutations)
    elif "cycles".startswith(mode):
        print_characteristic_cycles(characteristic_cycles(permutations))
    elif "lengths".startswith(mode):
        print_characteristic_cycle_length_pairs(characteristic_cycles(permutations))
    else:
        exit_usage()
