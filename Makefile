CC = clang
CXX = clang++
CCFLAGS = -Wall -Wextra -Og -pedantic -march=native -mtune=native -g -pg -std=c++20
#CCFLAGS = -Wall -Wextra -O3 -pedantic -march=native -mtune=native -std=c++20
CXXFLAGS = $(CCFLAGS)

DEPS = Makefile
OBJ = obj

$(OBJ)/%.o: %.c %.h $(DEPS)
	mkdir -p $(OBJ)
	$(CC) -c -o $@ $< $(CCFLAGS)

$(OBJ)/%.o: %.cpp %.hpp $(DEPS)
	mkdir -p $(OBJ)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

all: enigma-shell enigma-batch enigma-38 enigma-30 enigma-40

enigma-shell: enigma-shell.cpp $(OBJ)/enigma_args.o $(OBJ)/enigma_base.o
	$(CXX) $(CXXFLAGS) -lreadline enigma-shell.cpp $(OBJ)/enigma_args.o $(OBJ)/enigma_base.o -o enigma-shell

enigma-batch: enigma-batch.cpp $(OBJ)/enigma_args.o $(OBJ)/enigma_base.o
	$(CXX) $(CXXFLAGS) enigma-batch.cpp $(OBJ)/enigma_args.o $(OBJ)/enigma_base.o -o enigma-batch

enigma-30: enigma-30.cpp $(OBJ)/enigma_args.o $(OBJ)/enigma_base.o
	$(CXX) $(CXXFLAGS) enigma-30.cpp $(OBJ)/enigma_args.o $(OBJ)/enigma_base.o -o enigma-30

enigma-38: enigma-38.cpp $(OBJ)/enigma_args.o $(OBJ)/enigma_base.o
	$(CXX) $(CXXFLAGS) enigma-38.cpp $(OBJ)/enigma_args.o $(OBJ)/enigma_base.o -o enigma-38

enigma-40: enigma-40.cpp $(OBJ)/enigma_args.o $(OBJ)/enigma_base.o
	$(CXX) $(CXXFLAGS) enigma-40.cpp $(OBJ)/enigma_args.o $(OBJ)/enigma_base.o -o enigma-40


.PHONY: clean bin_clean obj_clean
clean: bin_clean obj_clean
bin_clean:
	rm -fv enigma-shell enigma-batch enigma-38 enigma-30 enigma-40
obj_clean:
	rm -rfv $(OBJ)/*
