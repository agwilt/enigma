#include "enigma_args.hpp"
#include "enigma_models.hpp"

#include <iostream>

bool isupperchar(std::string const& s)
{
	return std::isupper(s[0]) and (s[1] == '\0');
}

bool is_n_upper_chars(std::string const& s, int n)
{
	for (int i = 0; i < n; ++i) {
		if (not std::isupper(s[i]))
			return false;
	}
	return s[n] == '\0';
}

Enigma enigma_from_command_line_args(int argc, char *argv[], bool const expect_grundstellung)
{
	using namespace std::string_literals;
	auto const exit_printing_usage = [&]() {
		std::cerr << "Usage: " << argv[0] << " n Eintrittswalze Reflector [ReflectorPosition] "
		          << "Rotor₁ ... Rotorₙ RingPos₁...RingPosₙ ";
		if (expect_grundstellung) std::cerr << "StartingPos₁...StartingPosₙ ";
		std::cerr << "[SteckerPairs ...]\n"
		          << "Examples:\n";
		if (expect_grundstellung) std::cerr << "\t" << argv[0] << " 3 abc UKW-B III II I RNG QWE ST EC KR\n";
		else std::cerr << "\t" << argv[0] << " 3 abc UKW-B III II I RNG ST EC KR\n";
		exit(1);
	};

	if (argc == 1)
		exit_printing_usage();

	size_t const num_rotors = std::stoi(argv[1]);

	// Eintrittswalze
	char const* etw_name = argv[2];
	RotorType const* etw_type = NULL;
	switch (etw_name[0]) {
		case 'A':
		case 'a':
			// alphabetic Eintrittswalze, as used by German military Enigmas
			etw_type = &abc_etw;
			break;
		case 'Q':
		case 'q':
			// "QWERTZU..." Eintrittswalze, as used by civilian Enigmas
			etw_type = &qwertzu_etw;
			break;
		default:
			std::cerr << "Error: Unknown Eintrittswalze: " << etw_name << "\n";
			exit(1);
	}

	// Umkehrwalze
	char const* reflector_name = argv[3];
	RotorType const* reflector_type = NULL;
	bool settable_reflector = false;
	bool step_reflector = false;

	if (reflector_name == "UKW"s) { // Umkehrwalze in Enigma D, Enigma G, Enigma K and Swiss-K
		settable_reflector = true;
		reflector_type = &commercial_reflector;
	} else if (reflector_name == "UKW-A"s or reflector_name == "A"s) {
		reflector_type = &ukw_a;
	} else if (reflector_name == "UKW-B"s or reflector_name == "B"s) {
		reflector_type = &ukw_b;
	} else if (reflector_name == "UKW-C"s or reflector_name == "C"s) {
		reflector_type = &ukw_c;
	} else if (reflector_name == "Id"s) {
		reflector_type = &abc_etw;
	} else {
		std::cerr << "Error: Unknown Reflector: " << reflector_name << "\n";
		exit(1);
	}

	Enigma enigma{*etw_type, *reflector_type, step_reflector};

	if (settable_reflector) {
		if (not (isupperchar(argv[4]))) {
			std::cerr << "Error: Reflector position must be between 'A' and 'Z', not " << argv[4] << "\n";
			exit(1);
		}
		enigma.reflector.set_position(argv[4][0]);
	}

	// Othor rotors
	auto const rotor_choice = argv + 4 + settable_reflector;
	auto const ringstellung_str = rotor_choice[num_rotors];
	auto const grundstellung_str = expect_grundstellung ? rotor_choice[num_rotors + 1] : nullptr;
	auto const stecker_settings_beginning = 4 + settable_reflector + num_rotors + 1 + expect_grundstellung;
	if (static_cast<unsigned>(argc) < stecker_settings_beginning)
		exit_printing_usage();

	if (not is_n_upper_chars(ringstellung_str, num_rotors)) {
		std::cerr << "Ringstellung must be a number between A and Z, not " << ringstellung_str << "\n";
		exit(1);
	}
	if (expect_grundstellung and not is_n_upper_chars(grundstellung_str, num_rotors)) {
		std::cerr << "Rotor position must be a number between A and Z, not " << *grundstellung_str << "\n";
		exit(1);
	}

	for (unsigned i = 0; i < num_rotors; ++i) {
		char const ringstellung = ringstellung_str[i];
		char const initial_pos = expect_grundstellung ? grundstellung_str[i] : 'A';

		enigma.rotors.emplace_back(
			rotor_types.at(rotor_choice[i]),
			ringstellung,
			initial_pos
		);
	}

	for (int i = stecker_settings_beginning; i < argc; ++i) {
		char const* pair = argv[i];
		if (not (std::isupper(pair[0]) and std::isupper(pair[1]) and pair[2] == '\0')) {
			std::cerr << "Error: Stecker settings must be given a space-separated list of letter pairs.\n";
			continue;
		}
		enigma.steck(pair[0] - 'A', pair[1] - 'A');
	}

	return enigma;
}
