#ifndef ENIGMA_MODELS_H
#define ENIGMA_MODELS_H

#include "enigma_base.hpp"

#include <map>

/*
 * Eintrittswalzen
 */

static auto const qwertzu_etw = RotorType::etw_from_key_order("QWERTZUIOASDFGHJKPYXCVBNML");
static auto const abc_etw     = RotorType::etw_from_key_order("ABCDEFGHIJKLMNOPQRSTUVWXYZ");

/*
 * Umkehrwalzen
 */

static auto const commercial_reflector = RotorType::reflector("IMETCGFRAYSQBZXWLHKDVUPOJN");

static auto const ukw_a = RotorType::reflector("EJMZALYXVBWFCRQUONTSPIKHGD");

static auto const ukw_b = RotorType::reflector("YRUHQSLDPXNGOKMIEBFZCWVJAT");

static auto const ukw_c = RotorType::reflector("FVPJIAOYEDRZXWGCTKUQSBNMHL");

/*
 * Rotors
 */

static auto const commercial_rotor_i = RotorType::rotor("LPGSZMHAEOQKVXRFYBUTNICJDW", {'Z'});

static auto const commercial_rotor_ii = RotorType::rotor("SLVGBTFXJQOHEWIRZYAMKPCNDU", {'F'});

static auto const commercial_rotor_iii = RotorType::rotor("CJGDPSHKTURAWZXFMYNQOBVLIE", {'O'});

// Military

static auto const rotor_i = RotorType::rotor("EKMFLGDQVZNTOWYHXUSPAIBRCJ", {'R'});

static auto const rotor_ii = RotorType::rotor("AJDKSIRUXBLHWTMCQGZNPYFVOE", {'F'});

static auto const rotor_iii = RotorType::rotor("BDFHJLCPRTXVZNYEIWGAKMUSQO", {'W'});

static auto const rotor_iv = RotorType::rotor("ESOVPZJAYQUIRHXLNFTGKDCMWB", {'K'});

static auto const rotor_v = RotorType::rotor("VZBRGITYUPSDNHLXAWMJQOFECK", {'A'});

static std::map<std::string, RotorType const&> rotor_types{
	{"IC", commercial_rotor_i},
	{"IIC", commercial_rotor_ii},
	{"IIIC", commercial_rotor_iii},
	{"Id", abc_etw},
	{"I", rotor_i},
	{"II", rotor_ii},
	{"III", rotor_iii},
	{"IV", rotor_iv},
	{"V", rotor_v}
};

#endif // ENIGMA_MODELS_H
