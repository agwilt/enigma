#ifndef ENIGMA_BASE_H
#define ENIGMA_BASE_H

#include <array>
#include <vector>
#include <string>
#include <cstddef>

char get_key_letter();

// Internal position along circle. 0 is at the window, then 1 behind that, etc.
using Pos = short;
using PosArray = std::array<Pos, 26>;
using FlagArray = std::array<bool, 26>;

PosArray inverse(PosArray const& a);

constexpr char shift(char const c, unsigned short const i = 1) { return 'A' + ((c - 'A' + i) % 26); }

class RotorType {
public:
	RotorType(std::string const& wiring_str, std::vector<char> const& turnovers);
	RotorType(PosArray wiring, FlagArray turnovers);

	static RotorType etw_from_key_order(std::string const& inverse_wiring);
	static RotorType rotor(std::string const& wiring_str, std::vector<char> const& turnovers);
	static RotorType reflector(std::string const& wiring_str);

	static PosArray wiring_from_str(std::string const& s, Pos const pos_of_a_on_wheel);
	static FlagArray turnovers_from_letters(std::vector<char> const& s);
public:
	PosArray const wiring;
	PosArray const inverse_wiring;
	FlagArray const turnovers; // pre-turnover positions assuming trivial ring setting
};

class Rotor {
public:
	Rotor(
		RotorType const& type,
		Pos const ring_pos = 0,
		Pos const position = 0
	) : _type{type}, _ring_pos{ring_pos}, _position{position} {}

	Rotor(RotorType const& type, char const ring_setting, char const position) : _type{type}
	{
		set_ring(ring_setting);
		set_position(position);
	}

	void set_position(char letter_in_window);
	void set_ring(char ring_setting);

	// Actualy letter permutation methods, either fwd (right to left) or back (left to right)
	Pos fwd(Pos c) const;
	Pos back(Pos c) const;

	void step() { _position = (_position + 1) % 26; }

	constexpr auto const& type() const { return _type; }

	constexpr auto ring_pos() const { return _ring_pos; }
	constexpr char ring_setting() const { return 'A' + (26 - _ring_pos + 1) % 26; }

	constexpr auto position() const { return _position; }

	unsigned window() const; // i.e. 'A'+window() visible
	char window_letter() const { return 'A' + window(); }

	// Will be cause a turnover when we next move?
	bool turnover() const { return _type.turnovers.at(window()); }

public:
	RotorType const& _type;
	Pos _ring_pos; // position of "A"/"01" along rotor
	Pos _position; // position of window along rotor
};

struct Enigma {
public:
	Enigma(RotorType const& etw_type, RotorType const& ukw_type, bool step_reflector);

	char substitution(char c, bool detailed = false) const;

	char type(char c) { step(); return substitution(c); }
	void type_stream(FILE *in = stdin, FILE *out = stdout);

	void run();
	void step();

	void steck(Pos a, Pos b);
	bool unsteck(Pos a, Pos b); // returncode: success?

public:
	PosArray steckerbrett;
	Rotor eintrittswalze; // input as given to eintrittswalze is 0 for 'A', 1 for 'B', etc ...
	std::vector<Rotor> rotors; // rotors.front() is leftmost rotor, rotors.back() is rightmost
	Rotor reflector;
	bool step_reflector;
};

#endif // ENIGMA_BASE_H
