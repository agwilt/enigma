#include "enigma_base.hpp"
#include "enigma_args.hpp"

#include <iostream>
#include <sstream>
#include <iterator>

#include <cstdio>

#include <readline/readline.h>
#include <readline/history.h>

void run(Enigma &enigma)
{
	using namespace std::string_literals;
	auto const prompt = [&]() {
		std::string s{};
		//s += "["s + enigma.reflector.window_letter() + "] ";
		for (auto const& rotor : enigma.rotors) {
			s +=  "["s + rotor.window_letter() + "]";
		}
		return s + " ";
	};

	for (char *buf; (buf = readline(prompt().c_str())) != nullptr; free(buf)) {
		std::istringstream line{buf};
		std::vector<std::string> tokens{
			std::istream_iterator<std::string>{line},
			std::istream_iterator<std::string>{}
		};

		if (tokens.empty())
			continue;

		add_history(buf);

		auto const& command = tokens[0];
		auto const arg = [&](unsigned const i) -> std::string const& { return tokens[1 + i]; };

		if (command[0] == 'h') {
			std::cout << "Commands:\n";
			std::cout << " d[etailed] input_letter\n";
			std::cout << " h[help]\n";
			std::cout << " k[ey] show\n";
			std::cout << " k[ey] set pos|ring UKW|1|2|... new_position\n";
			std::cout << " p[os] new_position₁ ... new_positionₙ\n";
			std::cout << " static input_text\n";
			std::cout << " steck letter_pair₁ ...\n";
			std::cout << " s[tep] [n]\n";
			std::cout << " t[ype] input_text\n";
			std::cout << " unsteck letter_pair₂ ...\n";
		} else if (command == "step" or command == "s") {
			try {
				int const num_steps = (tokens.size() > 1) ? std::stoi(arg(0)) : 1;
				for (int i = 0; i < num_steps; ++i)
					enigma.step();
			} catch (std::invalid_argument const& e) {
				std::cerr << "Error: Invalid number of steps: " << arg(0) << "\n";
			}
		} else if (command[0] == 'k') {
			if (tokens.size() < 2) {
				std::cerr << "Error: Please use \"key show\" or \"key set ...\"\n";
				continue;
			} else if (arg(0) == "show") {
				// TODO: Give RotorType a name field
				std::cout << "Umkehrwalze: " << enigma.reflector.window_letter() << "\n";
				std::cout << "Ring Settings:";
				for (size_t i = 0; i < enigma.rotors.size(); ++i)
					std::cout << " " << enigma.rotors[i].ring_setting();
				std::cout << "\nRotor Positions:";
				for (size_t i = 0; i < enigma.rotors.size(); ++i)
					std::cout << " " << enigma.rotors[i].window_letter();
				std::cout << "\nSteckerbrett: ";
				for (int i = 0; i < 26; ++i) {
					if (enigma.steckerbrett.at(i) > i) {
						std::putchar('(');
						std::putchar('A' + i);
						std::putchar('A' + enigma.steckerbrett.at(i));
						std::putchar(')');
					}
				}
				std::putchar('\n');
			} else if (arg(0) == "set") {
				if (tokens.size() != 5) {
					std::cerr << "Error: Expecting 3 arguments to command \"key set\"\n";
					continue;
				}
				auto const& key_type = arg(1);
				auto const& rotor_name = arg(2);
				auto const& new_pos_name = arg(3);
				Rotor *rotor;

				if (not isupperchar(new_pos_name)) {
					std::cerr << "Error: New posiion must be an upper case letter.\n";
					continue;
				}

				if (rotor_name == "UKW") {
					rotor = &enigma.reflector;
					if (key_type == "ring") {
						std::cerr << "Error: Cannot set ring setting on Umkehrwalze\n";
						continue;
					}
				} else try {
					rotor = &enigma.rotors.at(std::stoi(rotor_name) - 1);
				} catch (std::exception const& e) {
					std::cerr << "Error: Please supply a rotor number (from the left) between 1 and "
					          << enigma.rotors.size() << "\n";
					continue;
				}

				if (key_type == "pos") {
					rotor->set_position(new_pos_name[0]);
				} else if (key_type == "ring") {
					rotor->set_ring(new_pos_name[0]);
				} else {
					std::cerr << "Error: Can only set \"pos\" or \"ring\"\n";
					continue;
				}
			} else {
				std::cerr << "Error: Unknown key command: " << arg(0) << "\n";
			}
		} else if (command[0] == 'p') {
			if (tokens.size() != 2) {
				std::cerr << "Error: Expecting 1 argument to command \"pos\"\n";
				continue;
			}
			if (arg(0).size() != 3) {
				std::cerr << "Error: Expecting " << enigma.rotors.size()
				          << " rotor positions in command \"pos\"\n";
				continue;
			}
			for (size_t i = 0; i < enigma.rotors.size(); ++i) {
				if (not std::isupper(arg(0)[i])) {
					std::cerr << "Error: New posiion must be an upper case letter, not " << arg(0)[i]
					          << ".\n";
					continue;
				}
				enigma.rotors[i].set_position(arg(0)[i]);
			}
		} else if (command[0] == 't') {
			for (auto token = tokens.begin() + 1; token != tokens.end(); ++token) {
				for (auto const& c : *token) {
					if (std::isupper(c)) {
						std::cout << enigma.type(c);
					} else if (std::islower(c)) {
						std::cout << enigma.type(c - 'a' + 'A');
					} else {
						std::cout << c;
					}
				}
				if ((token + 1) != tokens.end()) {
					std::cout << " ";
				}
			}
			std::cout << "\n";
		} else if (command == "static") {
			for (auto token = tokens.begin() + 1; token != tokens.end(); ++token) {
				for (auto const& c : *token) {
					if (std::isupper(c)) {
						std::cout << enigma.substitution(c);
					} else if (std::islower(c)) {
						std::cout << enigma.substitution(c - 'a' + 'A');
					} else {
						std::cout << c;
					}
				}
				if ((token + 1) != tokens.end()) {
					std::cout << " ";
				}
			}
			std::cout << "\n";
		} else if (command[0] == 'd') {
			if (not isupperchar(arg(0))) {
				std::cerr << "Error: Please supply a single upper-case letter\n";
			}
			enigma.substitution(arg(0)[0], true);
		} else if (command == "steck" or command == "unsteck") {
			for (size_t i = 0; i < tokens.size() - 1; ++i) {
				if (not (std::isupper(arg(i)[0]) and std::isupper(arg(i)[1]) and arg(i)[2] == '\0')) {
					std::cerr << "Error: Please give pairs of upper-case letters to be steckered.\n";
					continue;
				}
				char const a = arg(i)[0];
				char const b = arg(i)[1];
				if (command == "steck") {
					enigma.steck(a - 'A', b - 'A');
					std::cout << "Steckered " << a << " and " << b << " together\n";
				} else {
					if (enigma.unsteck(a - 'A', b - 'A'))
						std::cout << "Unsteckered " << a << " and " << b << "\n";
				}
			}
		} else {
			std::cerr << "Error: Unknown command: " << buf << ". Type \"help\" for a list of commands.\n";
		}
	}
};

int main(int argc, char *argv[])
{
	Enigma enigma = enigma_from_command_line_args(argc, argv, false);

	run(enigma);

	return 0;
}
