#!/bin/sh

NUM_MESSAGES=$2
ETW=ABC
UKW=UKW-A
ROTORS="I II III"
RINGSETTINGS="A A A"
GRUNDSTELLUNG="A A A"
STECKER="QW ER TY UI OP AS"

INNER_SETTINGS="3 $ETW $UKW $ROTORS $RINGSETTINGS"

echo "Generating $NUM_MESSAGES messages:"
echo ""
echo "###############################"
echo ""

for ((n=0; n < $NUM_MESSAGES; n++))
do
	KEY=$(pwgen -0 -A 3 1 | tr '[:lower:]' '[:upper:]')
	if [ "$1" = 30 ]; then
		echo $KEY$KEY | ./enigma-batch $INNER_SETTINGS $GRUNDSTELLUNG $STECKER
	elif [ "$1" = 38 ]; then
		GRUNDSTELLUNG=$(pwgen -0 -A 3 1 | tr '[:lower:]' '[:upper:]')
		echo "$GRUNDSTELLUNG"
		echo $KEY$KEY | ./enigma-batch $INNER_SETTINGS $(echo $GRUNDSTELLUNG | sed 's/./& /g') $STECKER
	elif [ "$1" = 40 ]; then
		GRUNDSTELLUNG=$(pwgen -0 -A 3 1 | tr '[:lower:]' '[:upper:]')
		echo "$GRUNDSTELLUNG"
		echo $KEY | ./enigma-batch $INNER_SETTINGS $(echo $GRUNDSTELLUNG | sed 's/./& /g') $STECKER
	else
		echo "Usage: $0 procedure num_messages"
		echo " procedure: key procedure, one of:"
		echo "  30: 1930 procedure, message key doubled and encrypted with day key."
		echo "  38: 1938 procedure, random Grundstellung used to encipher doubled message key. Grundstellung transmitted en clair in preamble."
		echo "  40: 1940 procedure, as 38 but message key only typed once."
		echo " num_messages: number of messages to generate"
		exit 1
	fi
	misfortune -m "^[^<]*$" | ./enigma-batch $INNER_SETTINGS $(echo $KEY | sed 's/./& /g') $STECKER
	echo ""
	echo "###############################"
	echo ""
done
