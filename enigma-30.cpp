#include "enigma_base.hpp"
#include "enigma_args.hpp"

#include <cstdio>
#include <iostream>

int main(int argc, char *argv[])
{
	Enigma enigma = enigma_from_command_line_args(argc, argv, true);

	std::vector<char> message_key(enigma.rotors.size(), '?');
	for (size_t i = 0; i < enigma.rotors.size(); ++i) {
		std::putchar(message_key[i] = enigma.type(get_key_letter()));
	}

	// Keys are still doubled
	for (size_t i = 0; i < enigma.rotors.size(); ++i) {
		char const plain_letter = enigma.type(get_key_letter());
		std::putchar(plain_letter);
		if (message_key[i] != plain_letter) {
			std::cerr << "\nError: Message key garbled\n";
			exit(1);
		}
	}

	// Set message key
	for (size_t i = 0; i < enigma.rotors.size(); ++i) {
		enigma.rotors[i].set_position(message_key[i]);
	}

	enigma.type_stream();

	return 0;
}
